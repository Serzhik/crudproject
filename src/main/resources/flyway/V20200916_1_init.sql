
DROP TABLE IF EXISTS fond;
create table fond (
                      id serial not null
                          constraint fond_pk
                              unique,
                      fond_number varchar default 255,
                      created_timestamp bigint not null ,
                      created_by bigint not null ,
                      updated_timestamp bigint,
                      updated_by bigint
);

alter table fond owner to postgres;

/*group_id bigint
                             constraint student_group_id_fk
                                 references groups (id)
*/
DROP TABLE IF EXISTS company;
create table company (
                         id serial not null
                         constraint company_pk
                             unique,
                         name_ru varchar default 128,
                         name_kaz varchar default 128,
                         name_en varchar default 128,
                         bin varchar default 32,
                         parent_id bigint,
                         fond_id bigint not null ,
                         created_timestamp bigint not null ,
                         created_by bigint not null ,
                         updated_timestamp bigint,
                         updated_by bigint

);

alter table company owner to postgres;

DROP TABLE IF EXISTS companyUnit;
create table companyUnit (
                         id serial not null
                         constraint companyUnit_pk
                             unique,
                         name_ru varchar default 128,
                         name_kaz varchar default 128,
                         name_en varchar default 128,
                         parent_id bigint,
                         year int,
                         company_id int,
                         code_index int,
                         created_timestamp bigint not null ,
                         created_by bigint not null ,
                         updated_timestamp bigint,
                         updated_by bigint

);

alter table companyUnit owner to postgres;