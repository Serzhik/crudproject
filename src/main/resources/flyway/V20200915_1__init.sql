
DROP TABLE IF EXISTS fond;
create table fond (
                      id serial not null
                          constraint fond_pk
                              unique,
                      fond_number varchar default 255,
                      created_timestamp bigint not null ,
                      created_by bigint not null ,
                      updated_timestamp bigint,
                      updated_by bigint
);

alter table fond owner to postgres;

/*group_id bigint
                             constraint student_group_id_fk
                                 references groups (id)
*/
DROP TABLE IF EXISTS company;
create table company (
                         id serial not null
                             constraint company_pk
                                 unique,
                         name_ru varchar default 128,
                         name_kaz varchar default 128,
                         name_en varchar default 128,
                         bin varchar default 32,
                         parent_id bigint,
                         fond_id bigint not null ,
                         created_timestamp bigint not null ,
                         created_by bigint not null ,
                         updated_timestamp bigint,
                         updated_by bigint

);

alter table company owner to postgres;


DROP TABLE IF EXISTS companyUnit;
create table companyUnit (
                             id serial not null
                                 constraint companyUnit_pk
                                     unique,
                             name_ru varchar default 128,
                             name_kaz varchar default 128,
                             name_en varchar default 128,
                             parent_id bigint,
                             year int,
                             company_id int,
                             code_index int,
                             created_timestamp bigint not null ,
                             created_by bigint not null ,
                             updated_timestamp bigint,
                             updated_by bigint

);

alter table companyUnit owner to postgres;


DROP TABLE IF EXISTS nomenclature;
create table nomenclature (
                             id serial not null
                                 constraint nomenclature_pk
                                     unique,
                             nomenclature_number varchar default 128,
                             year int,
                             nomenclature_summary_id bigint,
                             company_unit_id bigint,
                             created_timestamp bigint not null ,
                             created_by bigint not null ,
                             updated_timestamp bigint,
                             updated_by bigint

);

alter table nomenclature owner to postgres;


DROP TABLE IF EXISTS nomenclature_summary;
create table nomenclature_summary (
                         id serial not null
                             constraint nomenclature_summary_pk
                                 unique,
                         number varchar default 128,
                         year int,
                         company_unit_id bigint,
                         created_timestamp bigint not null ,
                         created_by bigint not null ,
                         updated_timestamp bigint,
                         updated_by bigint

);

alter table nomenclature_summary owner to postgres;


DROP TABLE IF EXISTS record;
create table record (
                      id serial not null
                          constraint record_pk
                              unique,
                      record_number varchar default 128,
                      type varchar default 128,
                      company_unit_id bigint,
                      created_timestamp bigint not null ,
                      created_by bigint not null ,
                      updated_timestamp bigint,
                      updated_by bigint
);

alter table record owner to postgres;


DROP TABLE IF EXISTS users;
create table users (
                       id serial not null
                           constraint users_pk
                               unique,
                       auth_id bigint,
                       name varchar default 128,
                       fullname varchar default 128,
                       surname varchar default 128,
                       secondname varchar default 128,
                       status varchar default 128,
                       company_unit_id bigint,
                       password varchar default 128,
                       last_login_timestamp bigint,
                       iin varchar default 128,
                       is_active bool,
                       is_activated bool,
                       created_timestamp bigint not null ,
                       created_by bigint not null ,
                       updated_timestamp bigint,
                       updated_by bigint

);

alter table users owner to postgres;


DROP TABLE IF EXISTS "case";
create table "case" (
                       id serial not null
                           constraint case_pk
                               unique,
                       auth_id bigint,
                       case_num varchar default 128,
                       house_num varchar default 128,
                       case_header_rus varchar default 128,
                       case_header_kaz varchar default 128,
                       case_header_en varchar default 128,
                       start_data bigint,
                       end_data bigint,
                       page_num bigint,
                       eds_signature_flag bool,
                       eds_signiture text,
                       naf_sending_sign bool,
                       deleting_sign bool,
                       engaged_access_flag bool,
                       hash varchar default 128,
                       version int,
                       version_id varchar default 128,
                       notes varchar default 128,
                       location_id bigint,
                       case_index_id bigint,
                       destruction_act_id bigint,
                       register_id bigint,
                       structure_unit_id bigint,
                       blockchain_case_address varchar default 128,
                       blockchain_add_data bigint,
                       created_timestamp bigint not null ,
                       created_by bigint not null ,
                       updated_timestamp bigint,
                       updated_by bigint

);

alter table "case" owner to postgres;


DROP TABLE IF EXISTS autorization;
create table autorization (
                       id serial not null
                           constraint autorization_pk
                               unique,
                       username varchar default 255,
                       email varchar default 255,
                       password varchar default 128,
                       role varchar default 255,
                       forgot_password_key varchar default 128,
                       forgot_password_key_timestamp bigint,
                       company_unit_id bigint

);

alter table autorization owner to postgres;


DROP TABLE IF EXISTS share;
create table share (
                              id serial not null
                                  constraint share_pk
                                      unique,
                              request_id bigint,
                              note varchar default 255,
                              sender_id bigint,
                              receiver_id bigint,
                              share_timestamp bigint

);

alter table share owner to postgres;


DROP TABLE IF EXISTS request;
create table request (
                       id serial not null
                           constraint request_pk
                               unique,
                       request_user_id bigint,
                       response_user_id bigint,
                       case_id bigint,
                       case_index_id bigint,
                       created_type varchar default 64,
                       comment varchar default 255,
                       status varchar default 64,
                       timestamp bigint,
                       sharestart bigint,
                       sharefinish bigint,
                       favourite bool,
                       updated_timestamp bigint,
                       updated_by bigint,
                       declinenote varchar default 255,
                       company_unit_id bigint,
                       from_request_id bigint


);

alter table request owner to postgres;


DROP TABLE IF EXISTS request_status_history;
create table request_status_history (
                                 id serial not null
                                     constraint request_status_history_pk
                                         unique,
                                 request_id bigint,
                                 status varchar default 64,
                                 created_timestamp bigint not null ,
                                 created_by bigint not null ,
                                 updated_timestamp bigint,
                                 updated_by bigint

);

alter table request_status_history owner to postgres;


DROP TABLE IF EXISTS case_index;
create table case_index (
                       id serial not null
                           constraint case_index_pk
                               unique,
                       case_index varchar default 128,
                       title_rus varchar default 128,
                       title_kaz varchar default 128,
                       title_en varchar default 128,
                       storage_type int,
                       storage_year int,
                       note varchar default 128,
                       company_unit_id bigint,
                       nomenclature_id bigint,
                       created_timestamp bigint not null ,
                       created_by bigint not null ,
                       updated_timestamp bigint,
                       updated_by bigint

);

alter table case_index owner to postgres;


DROP TABLE IF EXISTS file;
create table file (
                       id serial not null
                           constraint file_pk
                               unique,
                       name varchar default 128,
                       type varchar default 128,
                       size bigint,
                       page_count int,
                       hash varchar default 128,
                       is_deleted bool,
                       file_binary_id bigint,
                       created_timestamp bigint not null ,
                       created_by bigint not null ,
                       updated_timestamp bigint,
                       updated_by bigint

);

alter table file owner to postgres;


DROP TABLE IF EXISTS tempfile;
create table tempfile (
                      id serial not null
                          constraint tempfile_pk
                              unique,
                      file_binary text,
                      file_binary_byte bytea

);

alter table tempfile owner to postgres;


DROP TABLE IF EXISTS file_routing;
create table file_routing (
                          id serial not null
                              constraint file_routing_pk
                                  unique,
                          file_id bigint,
                          table_name varchar default 128,
                          table_id bigint,
                          type varchar default 128

);

alter table file_routing owner to postgres;


DROP TABLE IF EXISTS search_key;
create table search_key (
                      id serial not null
                          constraint search_key_pk
                              unique,
                      name varchar default 128,
                      company_unit_id bigint,
                      created_timestamp bigint not null ,
                      created_by bigint not null ,
                      updated_timestamp bigint,
                      updated_by bigint

);

alter table search_key owner to postgres;


DROP TABLE IF EXISTS search_key_routing;
create table search_key_routing (
                           id serial not null
                               constraint search_key_routing_pk
                                   unique,
                           search_key_id bigint,
                           table_name varchar default 128,
                           table_id bigint,
                           type varchar default 128

);

alter table search_key_routing owner to postgres;


DROP TABLE IF EXISTS notification;
create table notification (
                              id serial not null
                                  constraint notification_pk
                                      unique,
                              object_type varchar default 128,
                              object_id bigint,
                              company_unit bigint,
                              user_id bigint,
                              created_timestamp bigint not null ,
                              viewed_timestamp bigint not null ,
                              is_viewed bool,
                              title varchar default 255,
                              text varchar default 255,
                              company_id bigint


);

alter table notification owner to postgres;


DROP TABLE IF EXISTS location;
create table location (
                              id serial not null
                                  constraint location_pk
                                      unique,
                              row varchar default 64,
                              line varchar default 64,
                              columan varchar default 64,
                              box varchar default 64,
                              company_unit_id bigint,
                              created_timestamp bigint not null ,
                              created_by bigint not null ,
                              updated_timestamp bigint,
                              updated_by bigint

);

alter table location owner to postgres;


DROP TABLE IF EXISTS catalog;
create table catalog (
                          id serial not null
                              constraint catalog_pk
                                  unique,
                          name_rus varchar default 128,
                          name_kaz varchar default 128,
                          name_en varchar default 128,
                          parent_id bigint,
                          company_unit_id bigint,
                          created_timestamp bigint not null ,
                          created_by bigint not null ,
                          updated_timestamp bigint,
                          updated_by bigint

);

alter table catalog owner to postgres;


DROP TABLE IF EXISTS catalog_case;
create table catalog_case (
                         id serial not null
                             constraint catalog_case_pk
                                 unique,
                         case_Id bigint,
                         catalog_Id bigint,
                         company_unit_id bigint,
                         created_timestamp bigint not null ,
                         created_by bigint not null ,
                         updated_timestamp bigint,
                         updated_by bigint

);

alter table catalog_case owner to postgres;


DROP TABLE IF EXISTS activity_journal;
create table activity_journal (
                              id serial not null
                                  constraint activity_journal_pk
                                      unique,
                              event_type varchar default 128,
                              object_type varchar default 255,
                              object_id bigint,
                              created_timestamp bigint not null ,
                              created_by bigint not null ,
                              updated_timestamp bigint,
                              updated_by bigint

);

alter table activity_journal owner to postgres;


DROP TABLE IF EXISTS destruction_act;
create table destruction_act (
                                  id serial not null
                                      constraint destruction_act_pk
                                          unique,
                                  act_num varchar default 128,
                                  reason varchar default 128,
                                  structure_unit_id bigint,
                                  created_timestamp bigint not null ,
                                  created_by bigint not null ,
                                  updated_timestamp bigint,
                                  updated_by bigint

);

alter table destruction_act owner to postgres;
