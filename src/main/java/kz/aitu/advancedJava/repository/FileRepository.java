package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.entity.File;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository extends CrudRepository<File, Long> {


    @Query(value = "select * from file where id = 2", nativeQuery = true)
    File getFile();

    List<File> findAll();
}