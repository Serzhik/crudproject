package kz.aitu.advancedJava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Generated;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class File {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name ;

    private String type ;

    private long size ;

    private long page_count ;

    private String hash ;

    private boolean is_deleted ;

    private long file_binary_id ;

    private long created_timestamp  ;

    private long created_by   ;

    private long pdated_timestamp ;

    private long updated_by;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getPage_count() {
        return page_count;
    }

    public void setPage_count(long page_count) {
        this.page_count = page_count;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public boolean isIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    public long getFile_binary_id() {
        return file_binary_id;
    }

    public void setFile_binary_id(long file_binary_id) {
        this.file_binary_id = file_binary_id;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(long created_by) {
        this.created_by = created_by;
    }

    public long getPdated_timestamp() {
        return pdated_timestamp;
    }

    public void setPdated_timestamp(long pdated_timestamp) {
        this.pdated_timestamp = pdated_timestamp;
    }

    public long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(long updated_by) {
        this.updated_by = updated_by;
    }
}