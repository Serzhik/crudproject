package kz.aitu.advancedJava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Generated;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Request {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private long request_user_id ;

    private long response_user_id ;

    private long case_id ;

    private long case_index_id ;

    private String created_type  ;

    private String comment  ;

    private String status  ;

    private long timestamp ;

    private long sharestart ;

    private long sharefinish ;

    private boolean favourite ;

    private long updated_timestamp ;

    private long updated_by ;

    private String declinenote  ;

    private long company_unit_id ;

    private long from_request_id ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getRequest_user_id() {
        return request_user_id;
    }

    public void setRequest_user_id(long request_user_id) {
        this.request_user_id = request_user_id;
    }

    public long getResponse_user_id() {
        return response_user_id;
    }

    public void setResponse_user_id(long response_user_id) {
        this.response_user_id = response_user_id;
    }

    public long getCase_id() {
        return case_id;
    }

    public void setCase_id(long case_id) {
        this.case_id = case_id;
    }

    public long getCase_index_id() {
        return case_index_id;
    }

    public void setCase_index_id(long case_index_id) {
        this.case_index_id = case_index_id;
    }

    public String getCreated_type() {
        return created_type;
    }

    public void setCreated_type(String created_type) {
        this.created_type = created_type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getSharestart() {
        return sharestart;
    }

    public void setSharestart(long sharestart) {
        this.sharestart = sharestart;
    }

    public long getSharefinish() {
        return sharefinish;
    }

    public void setSharefinish(long sharefinish) {
        this.sharefinish = sharefinish;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public long getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(long updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(long updated_by) {
        this.updated_by = updated_by;
    }

    public String getDeclinenote() {
        return declinenote;
    }

    public void setDeclinenote(String declinenote) {
        this.declinenote = declinenote;
    }

    public long getCompany_unit_id() {
        return company_unit_id;
    }

    public void setCompany_unit_id(long company_unit_id) {
        this.company_unit_id = company_unit_id;
    }

    public long getFrom_request_id() {
        return from_request_id;
    }

    public void setFrom_request_id(long from_request_id) {
        this.from_request_id = from_request_id;
    }
}
