package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.entity.Users;
import kz.aitu.advancedJava.repository.UsersRepository;
import kz.aitu.advancedJava.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsersController {

    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/api/users/{usersId}")
    public ResponseEntity<?> getUsers(@PathVariable Long usersId) {
        return ResponseEntity.ok(usersService.getById(usersId));
    }

    @GetMapping("/api/users")
    public ResponseEntity<?> getUsers() {
        return ResponseEntity.ok(usersService.getAll());
    }

    @PostMapping("/api/users")
    public ResponseEntity<?> saveUsers(@RequestBody Users users) {
        return ResponseEntity.ok(usersService.create(users));
    }

    @PutMapping("/api/users")
    public ResponseEntity<?> updateUsers(@RequestBody Users users) {
        return ResponseEntity.ok(usersService.create(users));
    }

    @DeleteMapping("/api/users/{usersId}")
    public void deleteUsers(@PathVariable Long usersId) {
        usersService.delete(usersId);
    }
}
