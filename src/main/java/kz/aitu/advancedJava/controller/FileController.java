package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.entity.File;
import kz.aitu.advancedJava.repository.FileRepository;
import kz.aitu.advancedJava.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FileController {

    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/api/file/{fileId}")
    public ResponseEntity<?> getFile(@PathVariable Long fileId) {
        return ResponseEntity.ok(fileService.getById(fileId));
    }

    @GetMapping("/api/file")
    public ResponseEntity<?> getFile() {
        return ResponseEntity.ok(fileService.getAll());
    }

    @PostMapping("/api/file")
    public ResponseEntity<?> saveFile(@RequestBody File file) {
        return ResponseEntity.ok(fileService.create(file));
    }

    @PutMapping("/api/file")
    public ResponseEntity<?> updateFile(@RequestBody File file) {
        return ResponseEntity.ok(fileService.create(file));
    }

    @DeleteMapping("/api/file/{fileId}")
    public void deleteFile(@PathVariable Long fileId) {
        fileService.delete(fileId);
    }
}
